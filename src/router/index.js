import { createRouter, createWebHistory } from 'vue-router'

import Home from "../components/home/Index.vue"
import Cart from "../components/Cart/Index.vue"
import Login from "../components/Login/Index.vue"
import SingUp from "../components/singUp/Index.vue"

const routes = [

  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/Cart',
    name: 'Cart',
    component: Cart
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/singUp',
    name: 'singUp',
    component: SingUp
  }


]


const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
