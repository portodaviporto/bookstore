import { reactive } from "@vue/reactivity"

const getState = reactive({
  people: {
    nome: '',
    email: ''
  },
  ultimoProdutoEscolhido: {
    nome: 'Caixa de som',
    preco: '5.00',
    descricao: 'uma caisa de som',
    imagePath: '../../../assets/productsImages/SoundBox.jpeg',
    id: 0,
    quantidadeNoCarrinho: 0
  },
  produtos: [
    {
      nome: 'Caixa de som',
      preco: '5.00',
      descricao: 'uma caisa de som',
      imagePath: '../../../assets/productsImages/SoundBox.jpeg',
      id: 0,
      quantidadeNoCarrinho: 0,
    },
    {
      nome: 'mouse',
      preco: '5.00',
      descricao: 'uma caisa de som',
      imagePath: '/assets/productsImages/SoundBox.jpeg',
      id: 1,
      quantidadeNoCarrinho: 0,
    },
    {
      nome: 'Mesa',
      preco: '50.00',
      descricao: 'uma mesa de escritorio',
      imagePath: '@/assets/productsImages/SoundBox.jpeg',
      id: 2,
      quantidadeNoCarrinho: 0,
    },
    {
      nome: 'laptop',
      preco: '1500.00',
      descricao: 'uma laptop basico',
      imagePath: '@/assets/productsImages/SoundBox.jpeg',
      id: 3,
      quantidadeNoCarrinho: 0,
    },
    {
      nome: 'keyboard',
      preco: '5.00',
      descricao: 'uma teclado',
      imagePath: '@/assets/productsImages/SoundBox.jpeg',
      id: 4,
      quantidadeNoCarrinho: 0,
    },
    {
      nome: 'bed',
      preco: '5.00',
      descricao: 'uma cama',
      imagePath: '@/assets/productsImages/SoundBox.jpeg',
      id: 5,
      quantidadeNoCarrinho: 0,
    },
    {
      nome: 'contact lens',
      preco: '5.00',
      descricao: 'lentes de contato',
      imagePath: '@/assets/productsImages/SoundBox.jpeg',
      id: 6,
      quantidadeNoCarrinho: 0,
    },
    {
      nome: 'pants',
      preco: '58.00',
      descricao: 'uma calça',
      imagePath: '@/assets/productsImages/SoundBox.jpeg',
      id: 7,
      quantidadeNoCarrinho: 0,
    },
    {
      nome: 'bus',
      preco: '50000.00',
      descricao: 'uma onibus',
      imagePath: '@/assets/productsImages/SoundBox.jpeg',
      id: 8,
      quantidadeNoCarrinho: 0,
    },
    {
      nome: 'usb cable ',
      preco: '5.00',
      descricao: 'uma cabo usb',
      imagePath: '@/assets/productsImages/SoundBox.jpeg',
      id: 9,
      quantidadeNoCarrinho: 0,
    }

  ],
  produtosFiltrados: [],
  modalOpen: false,

})

const setState = (chave, propriedade, valor) => {
  getState[chave][propriedade] = valor
}
getState.produtosFiltrados = getState.produtos

const setFiltrados = (pfiltrados) => {
  getState.produtosFiltrados = pfiltrados
}

const setModal = (opend) => {
  getState.modalOpen = opend
}

const setQuantidade = (quantidade) => {
  getState.ultimoProdutoEscolhido.quantidadeNoCarrinho = quantidade
  console.log(quantidade)
}

const getProductById = (id) => {
  for (const produto in getState.produtos)
    if (produto == id) return getState.produtos[id]

  return getState.produtos[0]
}

const setUltimoProdutobyId = (id) => {
  getState.ultimoProdutoEscolhido = getProductById(id)
  setModal(true)
}

export {
  getState,
  setState,
  setFiltrados,
  setModal,
  getProductById,
  setUltimoProdutobyId,
  setQuantidade,
}